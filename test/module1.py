from firrtl_py import *

class module1(Module):
    def __init__(m):
        super().__init__("module1")
        m.clk = In(Clock)
        m.rst = In(UInt[1])

        m.u1 = In(UInt[8])
        m.u2 = In(UInt[8])
        m.urdy = Out(UInt[1])
        m.uen = In(UInt[1])

        m.q = Out(UInt[8])
        m.qrdy = Out(UInt[1])
        m.qen = In(UInt[1])

        x1 = m.Wire(UInt[2])
        x1 @= m.u1

        x2 = m.Reg(UInt[8], m.clk, m.rst, init=m.u1)
        x2 @= m.u1 + UInt[None](2)
        m.q @= x2

        with m.If(x1 != 2):
            m.urdy @= x1[1]
        with m.Elif(x1 == 0):
            m.urdy @= x1[0]
        with m.Else:
            m.urdy @= 0

        m.qrdy @= None


if __name__ == '__main__':
    import sys
    m = module1()
    #print(str(m.to_firrtl_proto()))
    sys.stdout.buffer.write(m._to_firrtl_proto().SerializeToString())