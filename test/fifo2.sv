module FIFO2_width5(
  input        clk,
  input        rst,
  input  [4:0] wdata,
  output       wrdy,
  input        wen,
  output       overflow,
  output [4:0] rdata,
  output       rrdy,
  input        ren,
  output       underflow
);
`ifdef RANDOMIZE_REG_INIT
  reg [31:0] _RAND_0;
  reg [31:0] _RAND_1;
  reg [31:0] _RAND_2;
  reg [31:0] _RAND_3;
  reg [31:0] _RAND_4;
`endif // RANDOMIZE_REG_INIT
  reg [4:0] d0; // @[fifo2.py 24:0]
  reg [4:0] d1; // @[fifo2.py 25:0]
  reg [1:0] dv; // @[fifo2.py 26:0]
  reg  ovf; // @[fifo2.py 28:0]
  reg  udf; // @[fifo2.py 29:0]
  wire [1:0] _GEN_0 = ren & ~wen ? 2'h3 : dv; // @[fifo2.py 26:0 56:0 57:0]
  wire [1:0] _GEN_2 = wen & ren ? dv : _GEN_0; // @[fifo2.py 26:0 54:0]
  wire  _GEN_15 = ~ren; // @[fifo2.py 25:0 51:0 52:0]
  wire  _GEN_18 = dv == 2'h3; // @[fifo2.py 26:0 59:0 60:0]
  wire  _GEN_20 = dv == 2'h0; // @[fifo2.py 46:0 47:0]
  assign wrdy = ~dv[1]; // @[fifo2.py 43:0]
  assign overflow = ovf; // @[fifo2.py 30:0]
  assign rdata = d0; // @[fifo2.py 41:0]
  assign rrdy = dv[0]; // @[fifo2.py 42:0]
  assign underflow = udf; // @[fifo2.py 31:0]
  always @(posedge clk) begin
    if (wen & dv == 2'h0) begin // @[fifo2.py 46:0]
      d0 <= wdata; // @[fifo2.py 47:0]
    end else if (dv == 2'h1) begin // @[fifo2.py 50:0]
      if (!(wen & _GEN_15)) begin // @[fifo2.py 51:0]
        if (wen & ren) begin // @[fifo2.py 54:0]
          d0 <= wdata; // @[fifo2.py 55:0]
        end
      end
    end else if (ren & _GEN_18) begin // @[fifo2.py 59:0]
      d0 <= d1; // @[fifo2.py 61:0]
    end
    if (!(wen & _GEN_20)) begin // @[fifo2.py 46:0]
      if (dv == 2'h1) begin // @[fifo2.py 50:0]
        if (wen & ~ren) begin // @[fifo2.py 51:0]
          d1 <= wdata; // @[fifo2.py 52:0]
        end
      end
    end
    if (rst) begin // @[fifo2.py 26:0]
      dv <= 2'h0; // @[fifo2.py 26:0]
    end else if (wen & _GEN_20) begin // @[fifo2.py 46:0]
      dv <= 2'h1; // @[fifo2.py 48:0]
    end else if (dv == 2'h1) begin // @[fifo2.py 50:0]
      if (wen & _GEN_15) begin // @[fifo2.py 51:0]
        dv <= 2'h3; // @[fifo2.py 53:0]
      end else begin
        dv <= _GEN_2;
      end
    end else if (ren & dv == 2'h3) begin // @[fifo2.py 59:0]
      dv <= 2'h1; // @[fifo2.py 60:0]
    end
    if (rst) begin // @[fifo2.py 28:0]
      ovf <= 1'h0; // @[fifo2.py 28:0]
    end else begin
      ovf <= ovf | wen & ~wrdy; // @[fifo2.py 33:0]
    end
    if (rst) begin // @[fifo2.py 29:0]
      udf <= 1'h0; // @[fifo2.py 29:0]
    end else begin
      udf <= udf | ren & ~rrdy; // @[fifo2.py 34:0]
    end
  end
// Register and memory initialization
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
`ifndef SYNTHESIS
`ifdef FIRRTL_BEFORE_INITIAL
`FIRRTL_BEFORE_INITIAL
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
`ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  d0 = _RAND_0[4:0];
  _RAND_1 = {1{`RANDOM}};
  d1 = _RAND_1[4:0];
  _RAND_2 = {1{`RANDOM}};
  dv = _RAND_2[1:0];
  _RAND_3 = {1{`RANDOM}};
  ovf = _RAND_3[0:0];
  _RAND_4 = {1{`RANDOM}};
  udf = _RAND_4[0:0];
`endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end // initial
`ifdef FIRRTL_AFTER_INITIAL
`FIRRTL_AFTER_INITIAL
`endif
`endif // SYNTHESIS
endmodule
