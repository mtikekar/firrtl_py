from firrtl_py import UInt, FixedPt, Module

assert UInt[5] is UInt[5]
assert UInt[None] is UInt[None]
assert UInt[5] is not UInt[None]
assert UInt[5](4) is not UInt[5](4)

UInt[5]._to_type_proto()
UInt[None]._to_type_proto()
UInt[5](5)._to_proto()
UInt[5]._as_ref(Module(), "name")._to_proto()

assert FixedPt[4, 2] is FixedPt[4, 2]

assert FixedPt[4, 2].from_float(0.5)._value == 2
assert FixedPt[4, 2].from_float(0.75).to_float() == 0.75
try:
    value_err = False
    FixedPt[4, 2].from_float(0.4)
except ValueError:
    value_err = True

assert value_err
FixedPt[4,5]._to_type_proto()
FixedPt[2,3](5)._to_proto()
