# FIRRTL-Py

A Python frontend for [FIRRTL](https://github.com/chipsalliance/firrtl) inspired by [Magma](https://github.com/phanrahan/magma) and [Amaranth](https://github.com/amaranth-lang) (previously nMigen).

## Why

Lots of new RTL languages: https://github.com/drom/awesome-hdl. Most are based in Python and they essentially have structural Verilog semantics. Lots of intermediate representations are also being developed. FIRRTL is a nice one with a well written [spec](https://github.com/chipsalliance/firrtl/blob/master/spec/spec.pdf) and a programmatic definition in [Protobuf](https://github.com/chipsalliance/firrtl/blob/master/src/main/proto/firrtl.proto).


## Key benefits

 - Modern IDE features (code completion, navigation, lint, formatting) are automatically available.
 - Python makes it easy to write complicated parametrized logic. E.g. [Litex](https://github.com/enjoy-digital/litex) and [Misoc](https://github.com/m-labs/misoc) for generating SoCs.

## Drawbacks

 - A DSL embedded in Python ends up with awkward syntax. AST parsing and rewriting is possible but that might break IDE features.

## Desired features

 - Clear demarcation between elaboration logic and hardware description. It should be very clear which expressions are evaluated at elab time and which are compiled to hardware.
 - A clean syntax for writing modules by hand as well as a simple object model for use in programmatic generation.
 - Errors and warnings from all steps such as elab, verilog gen, simulation should point back to source code.
 - Methods and interfaces like Bluespec (instead of input/output wires).
 - Generated RTL should be somewhat readable.
 - This repo should be a simple wrapper on FIRRTL so that end users (RTL designers) can find it easy to understand and hackable.
 - Multiple clock domains should be supported from the start.

## Optional ideas to explore

 - Types and typeclasses like Bluespec (pattern matching would be great to have but okay if not available)
 - Guarded atomic actions and sequential consistency are major plus
 - Explicit scheduling like Koika
 - StmtFSM

## Non-goals

 - Simulation in Python: I mainly want a Verilog generator. But interop with Cocotb would be interesting.

## Other references

https://circt.llvm.org/talks/