import contextlib
import collections
import os
from typing import TypeVar, Type

from . import types, expr, tracer, firrtl_pb2

# Save filenames in source info relative to this path
SOURCE_ROOT = os.environ.get("FIRRTL_PY_SOURCE_ROOT")
SOURCE_INFO = os.environ.get("FIRRTL_PY_SOURCE_INFO", "1") == "1"
CALL_DEPTH = 2
Firrtl = firrtl_pb2.Firrtl

# TODO: add typing annotations to below
# TODO: support connecting a Reg/Wire/Expression directly to output
# TODO: support splatting a bundle to input/output
T = TypeVar("T")
def In(type_: Type[T]) -> T:
    return type_._as_ref()


def Out(type_: Type[T]) -> T:
    return type_._as_ref(sink=True)


def get_source_lineno(call_depth):
    out = {}
    if SOURCE_INFO and call_depth > 0:
        # TODO: per module config for source info enable and call depths?
        filename, lineno = tracer.get_src_loc(call_depth)
        if SOURCE_ROOT is not None:
            filename = os.path.relpath(filename, SOURCE_ROOT)
        out["source_info"] = {"position": {"filename": filename, "line": lineno}}
    return out

WhenBlock = collections.namedtuple("WhenBlock", ["cond", "stmts", "srcinfo"])

class When:
    """A store for If-Elif-Elif-Else blocks. Top-level module block is also
    stored as a When(True) block.
    """
    def __init__(self):
        self.blocks: list[WhenBlock] = []

    def new_block(self, cond):
        self.blocks.append(WhenBlock(cond, [], get_source_lineno(CALL_DEPTH)))
        return self

    def append(self, stmt):
        self.blocks[-1].stmts.append(stmt)

    def last_statement(self):
        stmts = self.blocks[-1].stmts
        return None if len(stmts) == 0 else stmts[-1]

    def _to_proto(self) -> Firrtl.Statement:
        # convert Elif to nested Else-If and remove conditionals for True
        kw = {}
        def to_nested(blocks):
            if len(blocks) == 0:
                return []
            if len(blocks) == 1 and blocks[0].cond is True:  # Else statement
                return blocks[0].stmts
            blk = blocks[0]
            return [{**blk.srcinfo, "when": {
                "predicate": blk.cond,
                "consequent": blk.stmts,
                "otherwise": to_nested(blocks[1:])}}]

        kw = to_nested(self.blocks)[0]
        kw = expr.convert_refs_to_proto(kw)
        return Firrtl.Statement(**kw)


# TODO: cache module creation based on parameter values, or uniquify later based on content
class Module:
    def __init__(self, name=None):
        if name is None:
            cls = type(self)
            if cls != Module:
                name = cls.__module__ + '_' + cls.__qualname__
            else:
                name = tracer.get_var_name()
        for k,v in [
                ("_name", name),
                ("_components", {}),
                ("_ports", type(name + "_ports", (types.Bundle,), {})),
                ("_submodules_used", []),
                # initialize stack with top-level context
                ("_statement_context_stack", [When().new_block(True)])
                ]:
            object.__setattr__(self, k, v)
        # TODO: add default clock and reset
        # TODO: change this to build up the port bundle in one shot

    def _add_statement(self, **kw):
        kw.update(get_source_lineno(CALL_DEPTH))
        self._statement_context_stack[-1].append(kw)

    def __setattr__(self, key, value):
        if not hasattr(self, "_components"):
            raise AttributeError("_components not found. Perhaps a super().__init__(name) is missing.")

        if key in self._components:
            if value is not self._components[key]:
                raise NameError(f"Cannot reassign to {key}")
        elif isinstance(value, types.RefMixin) and hasattr(value, "_sink"):
            # is an IO signal
            self._components[key] = value
            self._ports.__annotations__[key] = type(value)
            if not value._sink:
                setattr(self._ports, key, "flip")
            value._name = key
            value._parent = self

        object.__setattr__(self, key, value)

    def _add_component(self, name, type_):
        # TODO: just rename instead
        if name in self._components:
            raise NameError(f"Cannot reassign to {name}")
        value = type_._as_ref(parent=self, name=name)
        self._components[name] = value
        return value

    def Reg(self, type_: Type[T], clk, rst=None, init=None, name=None) -> T:
        # TODO: connect default clock and reset
        # TODO: typecheck init
        # TODO: infer type_ from init's type
        name = tracer.get_var_name(CALL_DEPTH) if name is None else name
        rst = types.UInt[1](0) if rst is None else rst
        init = types.UInt[None](0) if init is None else type_._as_expr(init)
        self._add_statement(
                register={"id": name, "type": type_, "clock": clk, "reset": rst, "init": init})
        return self._add_component(name, type_)

    def Wire(self, type_: Type[T], name=None) -> T:
        name = tracer.get_var_name(CALL_DEPTH) if name is None else name
        self._add_statement(wire={"id": name, "type": type_})
        return self._add_component(name, type_)

    def Inst(self, submodule, name=None, **connections):
        # TODO: connect default clock and reset
        name = tracer.get_var_name(CALL_DEPTH) if name is None else name
        self._add_statement(instance={"id": name, "module_id": submodule})
        self._submodules_used.append(submodule)
        ports = self._add_component(name, submodule._ports)

        for port_name, sig in connections.items():
            port_sig = getattr(ports, port_name)
            lhs, rhs = (port_sig, sig) if port_sig._sink else (sig, port_sig)
            self._add_statement(connect={"location": lhs, "expression": rhs})
        return ports

    def _to_module_proto(self) -> Firrtl.Module:
        stmts = self._statement_context_stack[0].blocks[0].stmts
        stmts = expr.convert_refs_to_proto(stmts)
        return Firrtl.Module(user_module=Firrtl.Module.UserModule(
            id=self._name,
            port=self._ports._to_ports_proto(),
            statement=stmts))

    def _to_firrtl_proto(self) -> Firrtl:
        modules = all_submodules(self)
        return Firrtl(circuit=[Firrtl.Circuit(
            module=modules.values(),
            top=[Firrtl.Top(name=self._name)])])

    @contextlib.contextmanager
    def _in_when_block(self, when):
        # TODO: use try-finally as shown in examples to catch exceptions?
        self._statement_context_stack.append(when)
        yield
        self._statement_context_stack.pop()

    def If(self, expr):
        when = When()
        self._statement_context_stack[-1].append(when)
        return self._in_when_block(when.new_block(expr))

    def Elif(self, expr):
        when = self._statement_context_stack[-1].last_statement()
        if not isinstance(when, When):
            raise ValueError("Elif block without a preceding If.")
        return self._in_when_block(when.new_block(expr))

    @property
    def Else(self):
        when = self._statement_context_stack[-1].last_statement()
        if not isinstance(when, When):
            raise ValueError("Else block without a preceding If.")
        return self._in_when_block(when.new_block(True))


def all_submodules(module):
    out = {}
    def add_submodules(m):
        m_proto = m._to_module_proto()
        if m._name in out and out[m._name] != m_proto:
            raise ValueError(f"A different module named {m._name} already exists")
            # TODO: uniquify instead of error. to change name, need to traverse tree from the bottom
            # so that references in instances get the correct name
        out[m._name] = m_proto
        for mi in m._submodules_used:
            add_submodules(mi)

    add_submodules(module)
    return out
