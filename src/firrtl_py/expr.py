from typing import Optional, Union
import functools

from . import firrtl_pb2
from . import module

Firrtl = firrtl_pb2.Firrtl
Expr = Firrtl.Expression


def prim_op(op, *args, consts=[]):
    # TODO: check types and counts of args and const
    # TODO: propagate types to output for type-checking and width inference
    a0 = args[0]
    rest = [a0._as_expr(a) for a in args[1:]]
    return ExprMixin({"prim_op": {
        "op": "OP_" + op.upper(),
        "arg": [a0] + rest,
        "const": [integer_literal(c) for c in consts]}})

def prim_method(arg, op, *args, consts=[]):
    return prim_op(op, arg, *args, consts=[])

def prim(op):
    return functools.partialmethod(prim_method, op)


# TODO: make this immutable
class ExprMixin:
    """
    A Type object can be one of the following:
      Type(_value: int) - UInt, Int and FixedPoint literals
      Type(_value: dict) - An expression composed of other Type objects
      Type(_parent, _name: str, _sink: bool) - A reference to a circuit
        component such as an input/output port, register, wire, instance port,
        a field in a Bundle Type, an element in a Vector, etc.
    """
    _sink: bool  # signals are sources by default
    # TODO: check that either _value or _parent/_name are supplied
    # TODO: check if value is valid for the type
    # TODO: _sink is technically a class variable
    # TODO: see if a source_ is needed too
    # TODO: carry clock and reset domain info around with all signals

    def __init__(self, value: dict):
        # TODO: pure ExprMixin objects exist because type inference is not done
        self._value = value

    @classmethod
    def _as_expr(cls, value):
        if isinstance(value, int):
            return cls(value)
        elif isinstance(value, ExprMixin):
            return value
        raise TypeError(value)

    def __repr__(self):
        if isinstance(self._value, int):
            cls = type(self).__name__
            return f"{cls}({self._value})"
        if isinstance(self._value, dict):
            return f"Expr{self._value}"
        return super().__repr__()

    # TODO: this can be cached for speed
    def _to_proto(self) -> Expr:
        if isinstance(self._value, dict):
            kw = convert_refs_to_proto(self._value)
            return Expr(**kw)
        return super()._to_proto()

    __add__ = prim("add")
    __sub__ = prim("sub")
    __neg__ = prim("neg")

    __eq__ = prim("equal")
    __ne__ = prim("not_equal")
    __le__ = prim("less_eq")
    __lt__ = prim("less")
    __ge__ = prim("greater_eq")
    __gt__ = prim("greater")

    __invert__ = prim("bit_not")
    __and__ = prim("bit_and")
    __or__ = prim("bit_or")
    __xor__ = prim("bit_xor")

    def __getitem__(self, idx):
        if isinstance(idx, int):
            idx = slice(idx, idx+1, None)
        if isinstance(idx, slice):
            # TODO: bit-reverse using other ops
            if idx.step not in (1, None):
                raise ValueError(f"Step size {idx.step} != 1")
            start = 0 if idx.start is None else idx.start
            width = getattr(self, "_width", None)
            stop = width if idx.stop is None else idx.stop
            # width may be None (unknown) or an integer, but stop == width will
            # work correctly in either case
            if start == 0 and stop == width:
                return self
            # head: val[-n:], tail: [:-n]
            if start < 0 and stop == width:
                return prim_op('head', self, consts=[-start])
            if start == 0 and stop is not None and stop < 0:
                return prim_op('tail', self, consts=[-stop])
            elif start >= 0 and stop is not None and stop > start:
                # TODO: check if stop < width? how to handle unknown widths
                return prim_op('extract_bits', self, consts=[stop-1, start])
        raise TypeError(f"Invalid index {idx}")


def integer_literal(x: int) -> Expr.IntegerLiteral:
    return Expr.IntegerLiteral(value=str(x))


def big_int(x: int) -> Firrtl.BigInt:
    num_bytes = 1 + (x.bit_length() // 8)
    return firrtl_pb2.Firrtl.BigInt(value=x.to_bytes(num_bytes, "little", signed=True))


def width(w: Optional[int]) -> Optional[Firrtl.Width]:
    if w is not None:
        return Firrtl.Width(value=w)

def convert_refs_to_proto(obj):
    # TODO: doing this at the very end obscures the source of errors, but doing this as
    # the module is being constructed removes references to actual objects which
    # prevents renaming passes for example.
    if isinstance(obj, (list, tuple)):
        return [convert_refs_to_proto(v) for v in obj]
    if isinstance(obj, dict):
        return {k:convert_refs_to_proto(v) for k, v in obj.items()}
    if isinstance(obj, type) and hasattr(obj, "_to_type_proto"):
        return obj._to_type_proto()
    if hasattr(obj, "_to_proto"):
        return obj._to_proto()
    if isinstance(obj, module.Module):
        return obj._name  # instance statements use Modules by name "str"
    return obj
